'use strict'

var bcrypt = require('bcrypt-nodejs');
var mongoosePaginate = require('mongoose-pagination');
var fs = require('fs');
var path = require('path');

var User = require('../models/user');
var Follow = require('../models/follow');
var jwt = require('../services/jwt');






function inicio(req,res){
	 res.status(200).send({ a:   "userDetails" });
}



















//rutas
function test(req,res){
	//prueba(req,res);
	var userDetails;
	var initializePromise = initialize(req.user.sub);
    initializePromise.then(function(result) {
	        let userDetails = result;
	        console.log("Initialized user details");
	        // Use user details from here
	        console.log(userDetails);
	        res.status(200).send({
			message:userDetails
		});
    }, function(err) {
        console.log(err);
    });
};

function initialize(userid) {
	return new Promise(function(resolve, reject) {
		let document = Follow.findOne({'user':userid}).exec((err,followed)=>{
			if(err) reject(err);
			//console.log(followed);
			resolve(followed);
		});
	});
}

//https://medium.com/dev-bits/writing-neat-asynchronous-node-js-code-with-promises-32ed3a4fd098
function initialize2(userid) {
	return new Promise(function(resolve, reject) {
		let document = Follow.findOne({'user':userid}).exec((err,followed)=>{
			if(err) reject(err);
			//console.log(followed);
			resolve(followed);
		});
	});
}

async function prueba(req,res){
	let document
	try {
		document = await Follow.findOne({'user':req.user.sub}).exec((err,followed)=>{
			if(err) return res.status(500).send({message:'Error en la petición'})
			console.log(followed);
			return followed;
		});
		console.log(document);
	} catch (err) {
		console.log('Mongo error', err)
		return res.status(500).send();
	}

	//executeLogic(document, req, res);
	
}

module.exports={
	test,
	inicio
}